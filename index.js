//imports
const express = require('express');
const mongoose = require('mongoose')
const cors = require('cors');
const stripe = require("stripe")('sk_test_51IW3CZE0vIrZAQJ4MC9UFjTul1TSCeqpnJZijy1FlicaKJaJTOk4jFPIQvSolnT8V3pgtIdAGa9hTEUzaUWrUlDN00Vo89QTn7')
const OrderModel = require('./models/order.js');

const app = express();

app.use(express.json());//middleware
app.use(cors());
const port = process.env.PORT || 3001;

const connection_url =
    'mongodb+srv://ayushmehta651:wow9680552058@cluster0.kjwpu.mongodb.net/orders?retryWrites=true&w=majority';
mongoose.connect(connection_url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.get('/', (req, res) => res.send('hello world'));

app.get('/read', (req, res) => {

    OrderModel.find({}, (err, result) => {
        if (err)
            res.send(err);

        res.send(result)
    }).sort({ createdAt: -1 })
})

app.post('/insert', async (req, res) => {

    const basket = req.body.basket;
    const amount = req.body.amount;
    const createdAt = req.body.createdAt;

    const order = OrderModel({
        basket: basket,
        amount: amount,
        createdAt: createdAt
    });

    try {
        await order.save();
        res.send("order Inserted");
    } catch (err) {
        console.log(err);
    }

});

app.post("/payment", async (req, res) => {
    let { amount, id } = req.body
    try {
        const payment = await stripe.paymentIntents.create({
            amount,
            currency: "INR",
            description: "Shipped Company",
            payment_method: id,
            confirm: true
        })
        console.log("Payment", payment)
        res.json({
            message: "Payment successful",
            success: true,
            amount: payment.amount_received,
            createdAt: payment.created
        })
    } catch (error) {
        console.log("Error", error)
        res.json({
            message: "Payment failed",
            success: false
        })
    }
})

app.listen(port, () => {
    console.log(`server running on port ${port}`);
})